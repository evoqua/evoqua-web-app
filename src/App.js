import './App.css';

import 'materialize-css';
import 'materialize-css/dist/css/materialize.min.css'
import { Component } from 'react';
import {FooterComponent} from './components/FooterComponent'
import { HeaderComponent } from './components/HeaderComponent'
import { MainContent } from './components/MainContent'

export class App extends Component{
  render(){
  return (
    <div>
      <HeaderComponent/>      
      <MainContent/>
      <FooterComponent/>
    </div>
  );
}}

export default App;
