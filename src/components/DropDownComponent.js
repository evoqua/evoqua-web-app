
import 'materialize-css';
import 'materialize-css/dist/css/materialize.min.css'
import { Component } from 'react';


import { Dropdown, Button,} from 'react-materialize';


export class DropDownComponent extends Component{
  render(){
  return (
    <Dropdown
    id={this.props.id}
    style={{
      marginLeft: '30px'
    }}
    options={{
      alignment: 'left',
      autoTrigger: true,
      closeOnClick: true,
      constrainWidth: true,
      container: null,
      coverTrigger: true,
      hover: false,
      inDuration: 150,
      onCloseEnd: null,
      onCloseStart: null,
      onOpenEnd: null,
      onOpenStart: null,
      outDuration: 250
    }}
    trigger={<Button node="button">Drop Me!</Button>}
  >
    <li>fd</li>
  </Dropdown>
  );
}}

export default DropDownComponent;