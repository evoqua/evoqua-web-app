import 'materialize-css';
import 'materialize-css/dist/css/materialize.min.css'
import { SideNav, SideNavItem, Icon, Button } from 'react-materialize';
import { Component } from 'react';

export class SidenavComponent extends Component{
  render(){
  return (
    <div>
      <style>
      {`#root > div > div {
        z-index: 99999 !important;
        }
       `
      }
      </style>
      <SideNav
        id="SideNav-10"
        options={{
        draggable: true
        }}
        trigger={<Button
          style={{
            marginTop: '15px'
          }}
          className="pink"
          floating
          icon={<Icon>account_circle</Icon>}
          small
          node="button"
          waves="light"
        />}
      >
      <SideNavItem
        user={{
        background: 'https://placeimg.com/640/480/tech',
        email: 'jdandturk@gmail.com',
        image: 'static/media/react-materialize-logo.824c6ea3.svg',
        name: 'John Doe'
        }}
        userView
      />
      <SideNavItem
        href="#!icon"
        icon={<Icon>cloud</Icon>}
      >
      First Link With Icon
      </SideNavItem>
      <SideNavItem href="#!second">
        Second Link
      </SideNavItem>
    <SideNavItem divider />
    <SideNavItem subheader>
      Subheader
    </SideNavItem>
    <SideNavItem
      href="#!third"
      waves
    >
      Third Link With Waves
    </SideNavItem>
    </SideNav>
    </div>
  );
}}

export default SidenavComponent;