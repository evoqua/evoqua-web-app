import 'materialize-css';
import 'materialize-css/dist/css/materialize.min.css'
import { Component } from 'react';

import { GraphPlot } from './GraphPlot'

export class MainContent extends Component{
  render(){
  return (
    <GraphPlot/>
  );
}}

export default MainContent;