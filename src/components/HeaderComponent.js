import 'materialize-css';
import 'materialize-css/dist/css/materialize.min.css'
import {  Icon, Navbar, NavItem } from 'react-materialize';
import { Component } from 'react';

import { SidenavComponent } from './SidenavComponent'

export class HeaderComponent extends Component{
  render(){
  return (
    <div>
      <Navbar
        alignLinks="right"
        brand={<a className="brand-logo" href="#">Logo</a>}
        id="mobile-nav"
        menuIcon={<Icon>menu</Icon>}
        options={{
        draggable: true,
        edge: 'left',
        inDuration: 250,
        onCloseEnd: null,
        onCloseStart: null,
        onOpenEnd: null,
        onOpenStart: null,
        outDuration: 200,
        preventScrolling: true
        }}
      >
        
        <NavItem href="">
          Getting started
        </NavItem>
        <NavItem href="components.html">
          Components
        </NavItem>
        <SidenavComponent></SidenavComponent>
      </Navbar>
  </div>
  );
}}

export default HeaderComponent;