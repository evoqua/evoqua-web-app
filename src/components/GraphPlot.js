
import 'materialize-css';
import 'materialize-css/dist/css/materialize.min.css';
import { Component } from 'react';
import { DropDownComponent } from './DropDownComponent';
import ReactJSONEditor from './ReactJSONEditor.react.js';

import React from 'react';
import fetch from 'isomorphic-fetch';
import Plot from 'react-plotly.js';
//import createPlotlyComponent from 'react-plotly.js/factory';
import { Col, Container, Row } from 'react-materialize';

//const Plot = createPlotlyComponent(Plotly);

export class GraphPlot extends Component{

  constructor(props) {
    super(props);

    this.handleJsonChange = this.handleJsonChange.bind(this);
    this.getPlots = this.getPlots.bind(this);
    this.handleNewPlot = this.handleNewPlot.bind(this);
    
    const plotJSON = {
        data: [{
            x: [1,2,3,4],
            y: [1,3,2,6],
            type: 'bar',
            marker: {color: '#ab63fa'},
            name: 'Bar'
        }, {
            x: [1,2,3,4],
            y: [3,2,7,4],
            type: 'line',
            marker: {color: '#19d3f3'},
            name: 'Line'
        }],
        layout: {
            plotBackground: '#f3f6fa',
            margin: {t:0, r: 0, l: 20, b: 30},
        }
    };

    this.state = {
        json: plotJSON,
        plotUrl: ''
    };
}

handleJsonChange = newJSON => {
    this.setState({json: newJSON});
}

handleNewPlot = option => {
    let url = '';
    if ('value' in option) {
        url = option.value;
    }
    else if ('target' in option) {
        url = option.target.value;
        if (url.includes('http')) {
            if (!url.includes('.json')) {
                url = url + '.json'
            }
        }
    }

    if(url) {
        fetch(url)
        .then((response) => response.json())
        .then((newJSON) => {
            if ('layout' in newJSON) {    
                if ('height' in newJSON.layout) {
                    newJSON.layout.height = null;
                }
                if ('width' in newJSON.layout) {
                    newJSON.layout.width = null;
                }
            }
            this.setState({
                json: newJSON,
                plotUrl: url
            });
        });
    }
}

getPlots = (input) => {
    if (!input) {
  return Promise.resolve({ options: [] });
}

    let urlToFetch = `https://api.plot.ly/v2/search?q=${input}`;
    
return fetch(urlToFetch)
    .then((response) => response.json())
    .then((json) => {
      return { options: json.files.map(function(o) {
                return {
                    label: `${o.filename} by ${o.owner}, ${o.views} views`,
                    value: o.web_url.replace(/\/$/, "") + '.json'
                };
            })};
    });
};

getMocks = () => {
return fetch('https://api.github.com/repositories/45646037/contents/test/image/mocks')
    .then((response) => response.json())
    .then((json) => {
      return {
                complete: true,
                options: json.map(function(o) {
                    return {
                        label: o.name,
                        value: o.download_url
                    };
                })
            };
    });
};


  render(){
    return (
    <>
    <Row
      style={{
        marginTop: '10px'
      }}
    >
      <Col 
        s={9}
        offset = 's1'

      > 
        <DropDownComponent id="Dropdown_6"/>
      </Col>
      <Col
        s={2}
      >
        <DropDownComponent id="Dropdown_7"/>
      </Col>
      
    </Row>
    <div>
    <Row>
      <Col
        s={3}
      >
         <Plot
          data={this.state.json.data}
          layout={this.state.json.layout}
          config={{displayModeBar: false}}
        />
      </Col>
      <Col
        s={9}
      >
        <Plot
          data={this.state.json.data}
          layout={this.state.json.layout}
          config={{displayModeBar: false}}
        />
      </Col>
      
    </Row>     
    </div>
    </>
    
  );
}}

export default GraphPlot;